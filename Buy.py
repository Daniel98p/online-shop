import Product


class Buy:
    def __init__(self):
        self.laptop = Product.Product('Laptop', 1000, 'The best laptop in the world', 10)
        self.phone = Product.Product('Phone', 500, 'the best phone in the world', 20)
        self.chair = Product.Product('Chair', 100, 'the best chair in the world', 30)

    def products_list(self):
        list_of_products = [self.laptop.get_name(), self.phone.get_name(), self.chair.get_name()]
        return list_of_products
