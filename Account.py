import os
import Menu


class CreateAccount:
    def __init__(self):
        self.baskett = Menu.Menu()

    # def get_login(self):
    #     return self.login

    # def set_login(self, login):
    #     self.login = login

    def create_directory(self, login):
        try:
            os.mkdir(login)
            print('Your account is created, have a nice shopping!')
        except:
            print('You already have an account')

    def create_file(self, password, login):
        if len(os.listdir(f'C:/Users/danie/PycharmProjects/Online_shop/{login}')) < 1:
            filepath = os.path.join(f'C:/Users/danie/PycharmProjects/Online_shop/{login}', password)
            if not os.path.exists(f'C:/Users/danie/PycharmProjects/Online_shop//{login}'):
                os.makedirs(f'C:/Users/danie/PycharmProjects/Online_shop//{login}')
            open(filepath, "x")
        if os.listdir(f'C:/Users/danie/PycharmProjects/Online_shop//{login}')[0] != password:
            print('Wrong password')
            return False
        return True

    # def create_history_file(self, login):
    #     if len(os.listdir(f'C:/Users/danie/PycharmProjects/Online_shop/{login}')) == 1:
    #         filepath = os.path.join(f'C:/Users/danie/PycharmProjects/Online_shop/{login}', 'History.txt')
    #         if not os.path.exists(f'C:/Users/danie/PycharmProjects/Online_shop//{login}'):
    #             os.makedirs(f'C:/Users/danie/PycharmProjects/Online_shop//{login}')
    #         open(filepath, "x")

    def write_to_file(self, name, age, password, login):
        save_path = f'C:/Users/danie/PycharmProjects/Online_shop/{login}/{password}'
        file = open(save_path, "a")
        file.write(f"{name}\n")
        file.write(f"{str(age)}\n")
        file.write('Products added to basket:\n')
        for iterator in self.baskett.main_menu():
            file.write(f'{iterator}\n')
        file.close()

    def check_directory(self, login):
        if len(os.listdir(f'C:/Users/danie/PycharmProjects/Online_shop//{login}')) != 0:
            print("Directory is empty")
        else:
            print("Directory is not empty")
