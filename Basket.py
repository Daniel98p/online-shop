class Basket:
    def __init__(self):
        pass

    def add_to_basket(self, some_list, some_object):
        some_list.append(some_object)

    def show_list(self, some_list):
        print(some_list)

    def remove_from_list(self, some_list, some_object):
        some_list.remove(some_object)

    def delete_list(self, some_list):
        for iterator in some_list:
            some_list.remove(iterator)
