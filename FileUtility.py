file_name = 'History.txt'


def create_file():
    try:
        open(file_name, "x")
        print("Created file " + file_name)
    except:
        print("File exists")


def readFromFile():
    file = open(file_name, "r")
    print(file.read())
    file.close()


def writeToFile(some_object):
    file = open(file_name, "w")
    file.write(f'{some_object}\n')
    file.close()
