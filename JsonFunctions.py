import json


def convert_into_json(dic):
    result = json.dumps(dic)
    return result


def write_to_json_file(some_json):
    with open('my_list.txt', 'w') as outfile:
        json.dump(some_json, outfile)


def read_from_json_file():
    with open('my_list.txt') as json_file:
        my_list = json.load(json_file)
        print(my_list)

# def parse(some_json):
# result = json.loads(some_json)
# return result
