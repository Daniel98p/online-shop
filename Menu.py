import Buy
import JsonFunctions
import Basket
import FileUtility


class Menu:
    def __init__(self):
        self.purchase = Buy.Buy()
        self.basket_management = Basket.Basket()

    def main_menu(self):
        customer_basket = []
        exitt = True
        while exitt:
            try:
                choice = int(input('1 - buy\n2 - view the basket\n3 - show history of shopping\n4 - exit '))
                if choice == 1:
                    JsonFunctions.write_to_json_file(self.purchase.products_list())
                    JsonFunctions.read_from_json_file()
                    self.choose_product(customer_basket)
                    continue
                if choice == 2:
                    self.basket_management.show_list(customer_basket)
                if choice == 3:
                    FileUtility.readFromFile()
                if choice == 4:
                    return customer_basket
            except:
                print("Wrong choice")

    def choose_product(self, some_list):
        try:
            x = True
            while x:
                your_choice = input('Choose your product (L)aptop, (P)hone or (C)hair: ')
                if your_choice == 'L':
                    print('Name of product: ' + self.purchase.laptop.get_name())
                    print('Price of product: ' + str(self.purchase.laptop.get_price()))
                    print('Quantity of product: ' + str(self.purchase.laptop.get_quantity()))
                    print('Description of product: ' + self.purchase.laptop.get_description())
                    self.my_basket(some_list, self.purchase.laptop.get_name())
                    self.purchase_decision(self.purchase.laptop.get_name(), some_list)
                    x = False
                if your_choice == 'P':
                    print('Name of product: ' + self.purchase.phone.get_name())
                    print('Price of product: ' + str(self.purchase.phone.get_price()))
                    print('Quantity of product: ' + str(self.purchase.phone.get_quantity()))
                    print('Description of product: ' + self.purchase.phone.get_description())
                    self.my_basket(some_list, self.purchase.phone.get_name())
                    self.purchase_decision(self.purchase.phone.get_name(), some_list)
                    x = False
                if your_choice == 'C':
                    print('Name of product: ' + self.purchase.chair.get_name())
                    print('Price of product: ' + str(self.purchase.chair.get_price()))
                    print('Quantity of product: ' + str(self.purchase.chair.get_quantity()))
                    print('Description of product: ' + self.purchase.chair.get_description())
                    self.my_basket(some_list, self.purchase.chair.get_name())
                    self.purchase_decision(self.purchase.chair.get_name(), some_list)
                    x = False
        except:
            print('It is not a character')

    def my_basket(self, some_list, some_object):
        try:
            x = True
            while x:
                choice = input('Do you want to add the product to basket?\n1.(Y)es\n2.(N)o ')
                if choice == 'Y':
                    self.basket_management.add_to_basket(some_list, some_object)
                    x = False
                if choice == 'N':
                    print("You didn't add the product to basket")
                    x = False
        except:
            print('It is not a character')

    def purchase_decision(self, some_object, some_list):
        try:
            x = True
            while x:
                choice = input('Do you want to buy this product?\n1.(Y)es\n2.(N)o ')
                if choice == 'Y':
                    FileUtility.create_file()
                    FileUtility.writeToFile(some_object)
                    self.basket_management.delete_list(some_list)
                    print(some_list)
                    x = False
                if choice == 'N':
                    print("You didn't buy this product")
                    x = False
        except:
            print('It is not a number')
