class Product:
    def __init__(self, name, price, description, quantity):
        self.name = name
        self.price = price
        self.description = description
        self.quantity = quantity

    def get_name(self):
        return self.name

    def get_price(self):
        return self.price

    def get_description(self):
        return self.description

    def get_quantity(self):
        return self.quantity

    def set_price(self, price):
        self.price = price

    def set_quantity(self, quantity):
        self.quantity = quantity
